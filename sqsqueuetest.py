import boto3
import time
from datetime import datetime, timedelta
import logging

logging.basicConfig(format='%(asctime)s - %(message)s',filename="test.log", level=logging.INFO)

# Create SQS client
sqs = boto3.client('sqs')

queue_url = 'https://sqs.eu-west-2.amazonaws.com/702231975783/neilsfirstqueue.fifo'

def recieve():
    # Receive message from SQS queue
    response = sqs.receive_message(
        QueueUrl=queue_url,
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=1,
        MessageAttributeNames=[
            'All'
        ],
        VisibilityTimeout=10,
        WaitTimeSeconds=0
    )

    message = response['Messages'][0]
    

    receipt_handle = message['ReceiptHandle']

    # Delete received message from queue
    sqs.delete_message(
        QueueUrl=queue_url,
        ReceiptHandle=receipt_handle
    )

    #print('Received and deleted message: %s' % message)
    senttimestamp = message['Attributes']['SentTimestamp']
    countattribute = message['MessageAttributes']['count']['StringValue']
    messagebody = message['Body']

   
    

    humansenttimestamp = datetime(1970, 1, 1) + timedelta(seconds=int(senttimestamp)/1000)
    humandatetimenow = datetime(1970, 1, 1) + timedelta(seconds=int(datetime.now().strftime("%s")))

    d = datetime.strptime(str(datetime.now()), "%Y-%m-%d %H:%M:%S.%f").strftime('%s.%f')
    d_in_ms = int(float(d)*1000)

    difference = (humansenttimestamp - humandatetimenow).total_seconds()
    
    # print(senttimestamp) #from message attributes
    # print(humansenttimestamp) #human readable set from mesage attributes
    
    # print(d_in_ms) # datetime stamp now
    # print(datetime.fromtimestamp(float(d))) #human readable datetime stamp

    # print (int(d_in_ms) - int(senttimestamp)) #time difference

    
    # print (difference)


   
    print('{0} time taken in secs, {1} counts, {2} body, {3} original datetime sent'.format(difference, countattribute, messagebody, humansenttimestamp))

    logging.info('{0} time taken in secs, {1} counts, {2} body, {3} original datetime sent'.format(difference, countattribute, messagebody, humansenttimestamp))


while True:
    try:
        recieve()
    except:
        pass


