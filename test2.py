import boto3
import time
import uuid

# Create SQS client
sqs = boto3.client('sqs')

queue_url = 'https://sqs.eu-west-2.amazonaws.com/702231975783/neilsfirstqueue.fifo'

# Send message to SQS queue

def send(count):
    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageAttributes={
            'count': {
                'DataType': 'Number',
                'StringValue': f'{count}'
            },
        },
        MessageBody=(
            f'Message {uuid.uuid4()}'
        ),
        MessageGroupId=( 
            'messageGroup1'
        )
    )

    print(response['MessageId'])

count = 0

while count < 1000:
    send(count)
    print (f'The count is {count}')
    count = count +1